<?php
namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        
        $faker = Faker\Factory::create();
        for ($i=0; $i < 10; $i++) { 
            $customer = new Customer();
            $customer->setFirstName($faker->firstName);
            $customer->setLastName($faker->lastName);
            $customer->setEmail($faker->email);
            $customer->setContactNumber($faker->phoneNumber);
            $customer->setCompany($faker->company);
            $manager->persist($customer);
        }

        $manager->flush();
    }
}
