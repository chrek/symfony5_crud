<?php
namespace App\Controller;

use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends AbstractController
{
    private $customerRepository;
    
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }
    // Routes defined in Controller
    /**
     * @Route("/customers/add", name="add_customer", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $email = $data['email'];
        $contactNumber = $data['contactNumber'];
        $company = $data['company'];

        if (empty($firstName) || empty($lastName) || empty($email) || empty($contactNumber) || empty($company)) {
            throw new NotFoundHttpException('Parameters are mandatory!');
        } else {
            $this->customerRepository->saveCustomer($firstName, $lastName, $email, $contactNumber, $company);
            return new JsonResponse(['status' => 'Customer successfully created!'], Response::HTTP_CREATED);
        }
        
    }

    /**
     * @Route("/customers/{id}", name="get_one_customer", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);
        $data = [
            'id' => $customer->getId(),
            'firstName' =>  $customer->getFirstName(),
            'lastName' => $customer->getLastName(),
            'email' => $customer->getEmail(),
            'contactNumber' => $customer->getContactNumber(),
            'company' => $customer->getCompany(),
        ];
        return new JsonResponse($data, Response::HTTP_OK);        
    }

    /**
     * @Route("/customers", name="get_all_customer", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $customers = $this->customerRepository->findAll();
        $data = [];
        foreach ($customers as $customer) {
            $data[] = [
                'id' => $customer->getId(),
                'firstName' =>  $customer->getFirstName(),
                'lastName' => $customer->getLastName(),
                'email' => $customer->getEmail(),
                'contactNumber' => $customer->getContactNumber(),
                'company' => $customer->getCompany(),
            ];
        }        
        return new JsonResponse($data, Response::HTTP_OK);        
    }

    /**
     * @Route("/customers/update/{id}", name="update_customer", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);
        empty($data['firstName']) ?  true : $customer->setFirstName($data['firstName']);
        empty($data['lastName']) ?  true : $customer->setLastName($data['lastName']);
        empty($data['email']) ?  true : $customer->setEmail($data['email']);
        empty($data['contactNumber']) ?  true : $customer->setContactNumber($data['contactNumber']);
        empty($data['company']) ?  true : $customer->setCompany($data['company']);
        $updatedCustomer = $this->customerRepository->updateCustomer($customer);
        return new JsonResponse($updatedCustomer->toArray(), Response::HTTP_OK);        
    }

    /**
     * @Route("/customers/delete/{id}", name="delete_customer", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $customer = $this->customerRepository->findOneBy(['id' => $id]);
        $this->customerRepository->dropCustomer($customer);
        return new JsonResponse(['status' => 'Customer successfully deleted!'], Response::HTTP_NO_CONTENT);      
    }
}
