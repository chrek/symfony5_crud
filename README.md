# Symfony 5 CRUD Application

This CRUD application uses Symfony 5 framework.

The CRUD cycle with its method of functions can be used to enhance persistent storage in database records.

Although CRUD principles are mapped to REST by using GET, PUT, POST and CREATE, READ, UPDATE, DELETE, CRUD in itself is not REST but only comply with the goals of RESTful architecture.

The Project uses Faker to mock data into database

## Database
- Database: MariaDB 

## Environment Variables
.env: contains default values for the environment variables needed by the app
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7

## Controller
- CustomerController
- Routing is done in the Controller using Annotation

## Entity (represents a table in a relational database)
- Customer

## Mock Data
Run this command to mock data into database:
bin/console doctrine:fixtures:load

## Migration
Use the ommands below to generate and run your first migration version:
- bin/console make:migration
- bin/console doctrine:migrations:migrate

## Features
- Ability to perform CRUD (Create Read Update Delete) operations using database in MariaDB server

## Symfony Server
- Run the server and have it display the log messages in the console; you won’t be able to run other commands at the same time: 
  * symfony server:start
- Run the Symfony server in the background and continue working and running other commands:
  * symfony server:start -d

## Tested with Postman
- Create (C): localhost:8000/customers/add
- Read (R): 
  * localhost:8000/customers/{id} to get a customer with a given id and 
	* localhost:8000/customers to get all customers 
- Update (U):	localhost:8000/customers/update/{id} to update a customer with a given id
- Delete (D):	localhost:8000/customers/delete/{id} to delete a customer with a given id

## References
- [medium.com](https://medium.com/q-software/symfony-5-the-rest-the-crud-and-the-swag-7430cb84cd5)
- [bmc.com](https://www.bmc.com/blogs/rest-vs-crud-whats-the-difference/)